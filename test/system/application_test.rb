# frozen_string_literal: true

require 'application_system_test_case'

class ApplicationTest < ApplicationSystemTestCase
  test 'has a 404 page' do
    visit '/404'
    assert_selector 'h1', text: 'The page you were looking for doesn\'t exist.'
  end
end
