
# frozen_string_literal: true

class GroupFinderServiceTest < ActiveSupport::TestCase
  test 'provides a group' do
    user = FactoryBot.create :user
    location = FactoryBot.build :location
    group = GroupFinderService.new(user, location).group
    assert group.persisted?
  end
end
