# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::HitchhikersGuideToTheGalaxy.character }
  end
end
