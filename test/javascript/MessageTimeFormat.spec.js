import MessageTimeFormat from '../../app/javascript/lib/MessageTimeFormat';


const __test_options = {
  locale: "de",
  timeZone: "Europe/Vienna",
  today: new Date("2079-04-21")
}

test('Empty timestamp results in empty string', () => {
  let time = new MessageTimeFormat("");
  expect(time.formatted).toBeNull();
});


test("Timestamp will be converted to localized Date and Time", () => {
  let time = new MessageTimeFormat("1979-04-21T07:49:33.492Z", __test_options);
  expect(time.formatted).toBe("21.4.1979 08:48:33");
});


test("Timestamp from today will be converted to localized Time", () => {
  let time = new MessageTimeFormat("2079-04-21T07:49:33.492Z", __test_options);
  expect(time.formatted).toBe("08:48:33");
})
