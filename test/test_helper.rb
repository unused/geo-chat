# frozen_string_literal: true

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# Active Support
module ActiveSupport
  # Test Case
  class TestCase
    # Add more helper methods to be used by all tests here...
  end
end
