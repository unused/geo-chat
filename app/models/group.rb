# frozen_string_literal: true

# A chat group
class Group
  USERS_LIMIT = 10

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Geospatial

  field :location, type: Point, sphere: true

  embeds_many :messages
  has_many :users, dependent: :destroy

  # TODO: not happy with this naming
  scope(:with_space, -> { where :users_count.lte => USERS_LIMIT })
  scope(:with_location, -> { where :location.ne => nil })
end
