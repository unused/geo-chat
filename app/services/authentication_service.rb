# frozen_string_literal: true

class AuthenticationService
  def initialize(session)
    @session = session
  end

  def user
    if session?
      fetch_user
    else
      new_user
    end
  end

  private

  def session?
    @session[:user_id].present?
  end

  def fetch_user
    User.find @session[:user_id]
  rescue Mongoid::Errors::DocumentNotFound
    new_user
  end

  def new_user
    User.create!(name: Faker::Superhero.name).tap do |created_user|
      @session[:user_id] = created_user.id.to_s
    end
  end
end
