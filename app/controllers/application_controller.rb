# frozen_string_literal: true

# Base Application Controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_user

  private

  def set_user
    @user = AuthenticationService.new(cookies.signed).user
  end
end
