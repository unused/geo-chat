# frozen_string_literal: true

# Sessions Controller
class SessionsController < ApplicationController
  def destroy
    cookies.clear
  end
end
