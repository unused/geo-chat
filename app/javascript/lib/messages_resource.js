/**
 * Messages Resource utilizes data fetch, create, update and deletes.
 **/
class MessagesResource {
  get csrfToken() {
    if (!this._csrfToken) {
      this._csrfToken = document
        .querySelector('meta[name="csrf-token"]')
        .getAttribute('content');
    }
    return this._csrfToken;
  }

  create(data) {
    return fetch('/messages',
      this.options({ method: 'POST', body: this.formData(data) }));
  }

  formData(data) {
    const formData = new FormData();
    formData.append('authenticity_token', this.csrfToken);
    Object.keys(data)
      .forEach(key => formData.append(`message[${key}]`, data[key]));
    return formData;
  }

  options(options={}) {
    return Object.assign({}, {
      method: 'GET',
      headers: { 'Accept': 'application/json' },
      credentials: 'same-origin'
    }, options);
  }
}

export default MessagesResource;
