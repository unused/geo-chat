import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MessagesStore from 'lib/messages_store';

/**
 * Application fetches geolocation and initializes the chat
 **/
class Application extends Component {

  constructor(props) {
    super(props);
    this.state = { useRandom: false };
  }

  componentDidMount() {
    if ('geolocation' in navigator && window.isSecureContext) {
      navigator.geolocation.getCurrentPosition(
        position => this.setState({ coords: position.coords }),
        () => console.debug('unable to retrieve location')
      );
    } else {
      console.debug('join a random group, no geolocation available');
      this.setState({ useRandom: true });
    }
  }

  renderDialog() {
    return (
      <div className="location-dialog">
        <p>trying to fetch your position, you can <a href="#"
            onClick={() => this.setState({ useRandom: true })}>skip</a> to join
          a random group.</p>
      </div>
    );
  }

  render() {
    if (!this.state.useRandom && !this.state.coords) {
      return this.renderDialog();
    }

    return <MessagesStore coords={this.state.coords} />
  }
}

Application.propTypes = {};

export default Application;
