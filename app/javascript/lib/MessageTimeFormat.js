function _getLocale() {
  return this.locale;
}

function _getTimeZone() {
  return this.timeZone;
}

function _isTimestampFromToday(timestamp) {
  const date = new Date(timestamp).toDateString();

  // if options contains today key
  if(this.today) {
    return date == this.today.toDateString();
  } else {
    return date == new Date().toDateString();
  }
}


export default class MessageTimeFormat {

  constructor(timestamp, options) {
    this.timestamp = timestamp;
    this.options = options || {};
  }

  get formatted() {
    if(!this.timestamp) return null;

    const locale      = _getLocale.bind(this.options)();
    const tz          = {timeZone: _getTimeZone.bind(this.options)()};
    const date        = new Date(this.timestamp);
    const isToday     = _isTimestampFromToday.bind(this.options)(this.timestamp);
    let msg_formatted = "";


    // WTF - why does this not work?
    if ( !isToday )
      msg_formatted = date.toLocaleDateString(locale, tz) + " ";

    msg_formatted += date.toLocaleTimeString(locale, tz);

    return msg_formatted;
  }

}
