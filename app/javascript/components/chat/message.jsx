import React from 'react'
import PropTypes from 'prop-types'
import MessageTimeFormat from '../../lib/MessageTimeFormat'


function formatted_time(timestamp) {
  let mt = new MessageTimeFormat(timestamp.created_at);
  return mt.formatted;
}


const DefaultMessage = ({ content, username, created_at }) => (
  <p className="chat__message">
    <span className="chat__message-timestamp" title={created_at}>
      { formatted_time({created_at}) }
    </span>
    {username}: {content}
  </p>
);

const SelfMessage = ({ content, username }) => (
  <p className="chat__message chat__message--muted">
    {username} {content.substr(4)}
  </p>
);

const Message = (props) => {
  if (props.content.substr(0, 4) === '/me ') {
    return <SelfMessage {...props} />;
  }
  return <DefaultMessage {...props} />;
}

Message.propTypes = {
  username: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
}

export default Message;
