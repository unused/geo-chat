# GeoChat

Talk with people near your current location. A ruby on rails, action cable,
react chat application for educational purpose.

Users visiting the application will automatically become a user assigned, and
grouped together with some other users. In some (random) cases people will
start with a fresh group. We stick to the naming group but you can think of
a chat channel or chat room.

## Setup and Development

For a development environment have ruby (> "2.4.2") and gem [bundler]
installed. Ensure to have a [mongodb] and a [redis] server running and adapt
the configuration at `config/mongoid.yml` and `config/cable.yml` properly.

```sh
$ bundle install # install ruby libraries
$ bin/rails yarn:install # install JavaScript libraries
$ bin/rails db:mongoid:create_indexes # init database indexes
```

## Testing

Run tests using `bin/rails test`. We use [faker] to use nicer randomly
generated test data and [factory bot] to easy handle models.

```sh
$ bin/rails test # run unit tests
$ bin/rails test:system # run system tests using headless chrome
$ yarn test  # run JS unit tests
# do some static code analyizing (used in CI see .gitlab-ci.yml)
$ gem install rubocop && rubocop -D
$ gem install brakeman && brakeman
$ gem install bundle-audit && bundle-audit check --update
```

Note: System tests require Chrome to be installed.

[bundler]: http://bundler.io/ "The best way to manage ruby applications"
[mongodb]: https://www.mongodb.com/ "MongoDB"
[redis]: https://redis.io/ "Redis"
[faker]: https://github.com/stympy/faker "Faker ruby gem"
[factory bot]: https://github.com/thoughtbot/factory_bot_rails "Thoughtbots Factory Bot gem"
